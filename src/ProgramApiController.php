<?php declare(strict_types=1);

use App\InvestmentProject\Infrastructure\DAO\ProgramDAO;
use App\Shared\Application\Controller\MainController;
use Doctrine\DBAL\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ProgramApiController extends MainController
{
    public function __construct(private readonly ProgramDAO $dao)
    {
    }

    /**
     * @throws Exception
     */
    public function investProjectProgramList(Request $request): Response
    {
        $consolidationProgramId = $request->get('consolidationProgramId');
        $list = $this->dao
            ->listByConsolidationProgram(
                $this->prepareFirstChar($consolidationProgramId)
            );

        return $this->json($list);
    }

    /**
     * Костыль из-за несоответствия айдишников в двух таблицах. [SIP-144]
     * @param string $consolidationProgramId
     *
     * @return string
     */
    protected function prepareFirstChar(string $consolidationProgramId): string
    {
        return $consolidationProgramId === '8'
            ? '9'
            : $consolidationProgramId;
    }
}
