<?php declare(strict_types=1);

use App\BusinessCase\Infrastructure\DAO\B2C\B2cOtherServiceIncrementalRevenueDAO;
use App\Shared\Infrastructure\CommandBus\AttributesRestrictedHandler;
use App\Shared\Infrastructure\CommandBus\RoleBasedHandler;
use Doctrine\DBAL\Exception;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class ChangeB2cOtherServiceIncrementalRevenueHandler implements RoleBasedHandler, AttributesRestrictedHandler
{
    public function __construct(private readonly B2cOtherServiceIncrementalRevenueDAO $dao)
    {
    }

    public static function role(): string
    {
        return 'ROLE_B2C_OTHER_SERVICE_INCREMENTAL_REVENUE_EDITOR';
    }

    /** @inheritDoc */
    public static function attributes(): array
    {
        return ['IS_DRAFT_PROJECT'];
    }

    /** @throws Exception */
    public function __invoke(ChangeB2cOtherServiceIncrementalRevenue $command): array
    {
        $projectId = $command->projectId();
        $savedServiceIds = [];

        $this->dao->begin();
        foreach ($command->items() as $i => $item) {
            $serviceId = $item['serviceId'];

            if (array_key_exists('wasDeleted', $item) && $item['wasDeleted']) {
                $this->dao->delete($serviceId);
                continue;
            }

            $serviceId
                ? $savedServiceIds[$i] = $this->dao->update($serviceId, $projectId, $item)
                : $savedServiceIds[$i] = $this->dao->put($projectId, $item);
        }
        $this->dao->commit();

        return $savedServiceIds;
    }
}
