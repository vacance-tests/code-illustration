<?php declare(strict_types=1);

use App\Shared\Infrastructure\DAO\AppAbstractDAO;
use App\Whense\Application\DTO\WhenseCreateDTO;
use Doctrine\DBAL\Exception;

final class WhenseDAO extends AppAbstractDAO
{
    /**
     * @throws Exception
     */
    public function nextVal(): int
    {
        $sql = <<<SQL
            SELECT nextval('whence_id_seq')
        SQL;

        return (int) $this->db->fetchOne($sql);
    }

    /**
     * @throws Exception
     */
    public function save(WhenseCreateDTO $dto): void
    {
        $sql = <<<SQL
            INSERT INTO "whence".whence (type_id, category_id, "name", url, nickname, description)
                VALUES (
                        :b_type_id,
                        :b_category_id,
                        :b_name,
                        :b_url,
                        :b_nickname,
                        :b_description
                )
            ON CONFLICT (nickname) DO UPDATE
                SET "name" = :b_name,
                    description = :b_description,
                    created_at = CURRENT_TIMESTAMP
        SQL;

        $params = [
            'b_type_id' => $dto->getTypeId(),
            'b_category_id' => $dto->getCategoryId(),
            'b_name' => $dto->getName(),
            'b_url' => $dto->getUrl(),
            'b_nickname' => $dto->getNickname(),
            'b_description' => $dto->getDescription(),
        ];

        $this->db->executeQuery($sql, $params);
    }

    /**
     * @return array[array[
     *  'rss_bridge_name' => string,
     *  'nickname' => string,
     *  'whence_id' => int
     * ]]
     * @throws Exception
     */
    public function listForRssParsing(): array
    {
        $sql = <<<SQL
            SELECT 
                wh_t.rss_bridge_name, 
                wh.nickname,
                wh.whence_id
                FROM dictionary.whence_type wh_t
            JOIN whence.whence wh ON wh_t.type_id = wh.type_id;
        SQL;

        return $this->db->fetchAllAssociative($sql);
    }

    public function saveWithTag(WhenseCreateDTO $dto): void
    {
        $sql = <<<SQL
            WITH whence_insert AS 
                (INSERT INTO "whence".whence (type_id, category_id, "name", url, nickname, description)
                    VALUES (
                            :b_type_id,
                            :b_category_id,
                            :b_name,
                            :b_url,
                            :b_nickname,
                            :b_description
                    )
                ON CONFLICT (nickname) DO UPDATE
                    SET "name" = :b_name,
                        description = :b_description,
                        created_at = CURRENT_TIMESTAMP
                RETURNING whence_id AS whence_id_returned)
            
            --- Сохраняем связь с тегом
            INSERT INTO whence.tag_whence_link (whence_id, tag_id)
                SELECT whence_id_returned, :b_tag_id
                FROM whence_insert
            ON CONFLICT (whence_id, tag_id) DO NOTHING
        SQL;

        $params = [
            'b_type_id' => $dto->getTypeId(),
            'b_category_id' => $dto->getCategoryId(),
            'b_name' => $dto->getName(),
            'b_url' => $dto->getUrl(),
            'b_nickname' => $dto->getNickname(),
            'b_description' => $dto->getDescription(),
            'b_tag_id' => $dto->getTagId(),
        ];

        $this->db->executeQuery($sql, $params);
    }
}
