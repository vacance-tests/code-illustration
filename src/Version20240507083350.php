<?php declare(strict_types=1);
/** @noinspection PhpUnused */

use App\Shared\Infrastructure\Migration\AppAbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class Version20240507083350 extends AppAbstractMigration
{
    public function getDescription(): string
    {
        return 'Добавляет таблицу логов выполнения команд';
    }

    public function up(Schema $schema): void
    {
        $multiSql = <<<SQL
            CREATE SCHEMA "analytics";

            CREATE SEQUENCE "command_log_id_seq" INCREMENT BY 1 MINVALUE 1 START 1;

            CREATE TABLE analytics.command_log (
                command_log_id INT PRIMARY KEY DEFAULT NEXTVAL('command_log_id_seq'),
                command_name VARCHAR(255) NOT NULL,
                command_description VARCHAR(255) NOT NULL,
                started TIMESTAMP NOT NULL,
                ended TIMESTAMP,
                interval INTERVAL DEFAULT '0 seconds',
                message VARCHAR(500)
            );

            COMMENT ON TABLE analytics.command_log IS 'Логи выполнения консольных команд';

            COMMENT ON COLUMN analytics.command_log.command_log_id IS 'Уникальный идентификатор Лога';
            COMMENT ON COLUMN analytics.command_log.command_name IS 'Название команды';
            COMMENT ON COLUMN analytics.command_log.command_description IS 'Описание команды';
            COMMENT ON COLUMN analytics.command_log.started IS 'Время запуска команды';
            COMMENT ON COLUMN analytics.command_log.ended IS 'Время окончания выполнения команды';
            COMMENT ON COLUMN analytics.command_log.interval IS 'Время работы команды';
            COMMENT ON COLUMN analytics.command_log.message IS 'Возможное сообщение об ошибки выполнения команды';
        SQL;

        $this->addMultiSql($multiSql);
    }
}
