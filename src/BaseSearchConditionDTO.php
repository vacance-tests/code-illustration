<?php declare(strict_types=1);

namespace App\Domain\OEDK\Share\DTO;

use OpenApi\Attributes\Property;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Type;

abstract class BaseSearchConditionDTO
{
    public function __construct(
        #[
            Type('string'),
            Length(max: 250),
            Property(
                title: 'Поисковый запрос',
                type: 'string',
                example: 'договору',
            )
        ]
        private readonly ?string $search = null,
    ) {
    }

    public function getSearch(): ?string
    {
        return $this->search;
    }
}
