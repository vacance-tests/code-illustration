<?php

declare(strict_types=1);

namespace App\Domain\OEDK\Share\DTO;

use App\Domain\OEDK\Share\Entity\SortOrder;
use OpenApi\Attributes\Property;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Type;

class SortConditionDTO
{
    public function __construct(
        #[
            Type('string'),
            Length(max: 250),
            Property(
                title: 'Наименование поля для сортировки',
                type: 'string',
                example: 'name',
            )
        ]
        private readonly ?string $sortField = null,
        #[
            Assert\Choice(callback: [SortOrder::class, 'getArrayValues'], strict: true),
            Assert\Type('string'),
            Property(
                title: 'Направление сортировки (ASC/DESC)',
                type: 'string',
                example: 'DESC',
            )
        ]
        private readonly ?string $sortOrder = null,
    ) {
    }

    public function getSortField(): ?string
    {
        return $this->sortField;
    }

    public function getSortOrder(): ?string
    {
        return $this->sortOrder;
    }

    public function empty(): bool
    {
        return empty($this->sortField) || empty($this->sortOrder);
    }
}
